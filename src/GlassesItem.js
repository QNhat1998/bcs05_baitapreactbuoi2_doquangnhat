import React, { Component } from 'react'

export default class GlassesItem extends Component {
  render() {
    return (
      <img src={this.props.glasses.url} className='img-fluid' onClick={()=>this.props.changeGlasses(this.props.glasses)} alt="Glasses" />
    )
  }
}
