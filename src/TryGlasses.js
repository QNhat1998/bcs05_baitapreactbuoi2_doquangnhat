import React, { Component } from 'react'
import GlassesList from './GlassesList'
import dataGlasses from './asset/data/dataGlasses.json'

export default class TryGlasses extends Component {

  state = {
    glasses: {
      "id": 1,
      "price": 30,
      "name": "GUCCI G8850U",
      "url": "./glassesImage/v1.png",
      "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
    }
  }

  changeGlasses = (newGlasses) => {
    this.setState({
      glasses: newGlasses
    })
  }

  render() {
    return (
      <div className="content">
        <div style={{
          background: 'rgba(0,0,0,0.7)',
          padding: 40
        }}>
          <h1 className='text-center text-white'>TRY GLASSES APP ONLINE</h1>
        </div>
        <div className="container">
          <div className="model d-flex justify-content-around">
            <img src="./glassesImage/model.jpg" alt="" style={{
              width: '350px',
              height: '400px'
            }} />
            <img src="./glassesImage/model.jpg" alt="" style={{
              width: '350px',
              height: '400px',
              position: 'relative'
            }} />
            <div className='info_glasses p-3' style={{
              position: 'absolute',
              background: 'rgba(0,0,0,0.4)',
              bottom: '392px',
              right: '460px',
              width: '350px',
              height: '150px'
            }}>
              <h3 className='text-danger'>{this.state.glasses.name} </h3>
              <p className='text-white'>{this.state.glasses.desc} </p>
            </div>
            <img src={this.state.glasses.url} style={{
              position: 'absolute',
              top: '237px',
              right: '545px',
              width: '180px',
              height: '50px',
              opacity: 0.6}} alt="" />
          </div>
          <div className="glasses_list p-3 mt-5" style={{
            border: '2px solid black',
            borderRadius: '10px'
          }}>
            <GlassesList glasses={dataGlasses} changeGlasses={this.changeGlasses}/>
          </div>
        </div>
      </div>
    )
  }
}
