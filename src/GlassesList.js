import React, { Component } from 'react'
import GlassesItem from './GlassesItem'

export default class GlassesList extends Component {
  render() {
    let {glasses} = this.props

    const renderGlasses = () =>{
        return glasses.map((item,index)=>{
            return <div className="col-2" key={index}>
              <GlassesItem glasses={item} changeGlasses={this.props.changeGlasses}/>
            </div>
        })
    }

    return (
      <div className="row">
        {renderGlasses()}
      </div>
    )
  }
}
